


# Creek

Creek is a minimalistic functional reactive streams library.

## Motivation and Goals

Creek is created with the intent of condensing the powerful concepts of the functional reactive programming in a compact, and handy package. The project is aimed at delivering an ad-hoc, independent, extensible, lightweight, and functional API for building reactive applications.

## Overview

The functional reactive programming is a programming pattern (or a paradigm if you like) that can be summarized to: abstracting the I/O as a sequence of events and implementing the logic of the program in a declarative way by using the tools of the functional programming - map, filter, reduce, etc.

A Creek application is constructed with three types of building blocks: *source*, *stream*, and *operations*. The *operations* are just functions that follow a specific convention. They receive an event payload, or *message*, as an argument and are expected to return a processed result. The *stream* organizes the *operations* in an ordered list, in which every *operation* passes the result of its execution to the next. Finally, the *source* is the entity that injects events into the stream.

## Getting Started

### Installation
```
npm i event-creek
```

### Example usage
```javascript
import {stream, reduce, map} from 'event-creek';
```
Assuming there are some controls in the view:
```html
<button id="increment">+</button>
<span id="count">0</span>
<button id="decrement">-</button>
```
Define the actions of the buttons.
```javascript
const incrementer = count => count + 1;
const decrementer = count => count - 1;
```
Create an event source that puts actions into the stream when the corresponding buttons are clicked.
```javascript
function source(dispatch) {
	document.getElementById('increment').addEventListener('click', () => dispatch(incrementer));
	document.getElementById('decrement').addEventListener('click', () => dispatch(decrementer));
}
```
Define a stream constructed from the source and a *reduce* operation that will maintain the state (i.e. the count).

On every button click an action is be sent trough the stream. The reduce operation executes the `applyAction` callback over the current count and every passing action. The result is used to overwrite the old state and then passed along the stream.

As a demonstration chain an operation that returns a formatted string out of the raw count number.
```javascript
const applyAction = (count, action) => action(count);
const toText = count => `The current count is ${count}`;
const initialCount = 0;

const counter = stream(source, reduce(applyAction, initialCount), map(toText));
```
Run the stream by attaching an (optional)  function that renders the state.
```javascript
const renderer = count => document.getElementById('count').innerHTML = count;
counter(renderer);
```
A stream may be used as a source for another stream too.
```javascript
const example = stream(counter, map(text => text.toUpperCase()))
example(renderer);
```

## API reference
For the examples below, the following definitions are in place:
```javascript
function source(dispatch) {
	dispatch(1);
	dispatch(2);
	dispatch(3);
}
```
And the example streams are executed with:
```javascript
example(x => console.log(x));
```
### **filter(condition)**
Returns a filtering operation that will run the expression *condition(message)* on every incoming message and will pass back into the stream only if the result of the expression is true.
```javascript
const example = stream(source, filter(x => x > 1));
```
Result:
```
2
3
```

### **flatMap(rule)**
Creates an operation that will runs *rule(message)* on every message and (separately) injects the items of the resulting array into the stream.
```javascript
const example = stream(source, flatMap(x => [x, -x]));
```
Result:
```
1
-1
2
-2
3
-3
```
### **map(rule)**
Creates an operation that will runs *rule(message)* on every message and injects the result (as is) into the stream.
```javascript
const example = stream(source, map(x => x * 2));
```
Result:
```
2
4
6
```
### **reduce(rule, initial)**
Returns an operation that applies the given *rule* function against an accumulator and each incoming message and inject the result back into the stream. On the first run, the *rule* function will receive the *initial* value as a value of the accumulator. On the subsequent runs the value of the accumulator will be the result of the previous execution.
```javascript
const rule = (accumulator, x) => accumulator + x;
const example = stream(source, reduce(rule, 10));
```
Result:
```
11
13
16
```
### **stream(source, ...operations)**
Constructs a stream function from the given *source* and a list of *operations*. The *source* is expected to be a function that receives a single argument when the stream is executed, The *source*'s argument is itself a function that expects a single argument. Calling that *dispatch* function will inject the argument into the stream.
```javascript
function source(dispatch) {
	dispatch('message');
}
```
The operations are functions with two arguments *operation(message, next)*, where *message* is the message that is passed down the stream and *next* is a callback function, *next(message)*, that may to be called by the *operation* to pass a message down the stream.
```javascript
function operation(message, next) {
	next('modified ' + message);
}
```
Creek provides operation factories for the traditional operations on sequences: *map*, *flatMap*, *filter*, and *reduce*.

The result stream function must be executed in order to "run" the stream. It is an impure operation so it is preferable to do it once, after the whole program is defined. Running the stream will execute the source and provide it with a *dispatch* function. It can also attach an **optional** *sink* operation that will be attached to the end of the stream. The *sink* will not receive a *next* callback, just a *message* argument.
```javascript
function sink(message) {
	console.log(message);
}

const example = stream(source, operation);

example(sink);
```
Result:
```
modified message
```
### Building

Creek's development environment depends on having **[npm](https://www.npmjs.com/get-npm)** and **[git](https://git-scm.com/downloads)** installed.

Grab a copy of the repository.
```
git clone https://bitbucket.org/zmei-zelen/creek.git
```
Navigate in the newly created directory and install the dependencies.
```
cd creek
npm install
```
Generate a bundled ES6 module.
```
npm run build-es
```
The module will be created as *creek.es.js* in the *lib* directory.

It is also possible to build a distribution package.
```
npm run build-dist
```
A compressed, production-ready UMD module *creek.umd.min.js* will be generated in *dist*. An uncompressed version will also be available as *creek.umd.js* in *lib*.

The build scripts depend on **[rollup](https://rollupjs.org/guide)**, so with a little bit of tweaking various types of modules could be generated.

## Testing

Clone the repository and install the dependencies if needed.
```
git clone https://bitbucket.org/zmei-zelen/creek.git
cd creek
npm install
```
Then execute the tests:
```
npm test
```
Or generate a test coverage report:
```
npm run coverage
```

## License

This project is licensed under the MIT License.
