/*global expect, jasmine*/

import { stream } from '../src/stream';

describe('stream', () => {
    let source, operationA, operationB, sink;

    beforeEach(() => {
        source = jasmine.createSpy('source').and.callFake(dispatch => {
            dispatch(1);
            dispatch(2);
        });
        operationA = jasmine.createSpy('operationA').and.callFake((value, next) => next(value * 2));
        operationB = jasmine.createSpy('operationA').and.callFake((value, next) => next(value * 3));
        sink = jasmine.createSpy('sink');
    });

    it('calls the source when the stream is started', () => {
        const test = stream(source);
        expect(source).not.toHaveBeenCalled();

        test(sink);
        expect(source.calls.count()).toBe(1);
    });

    it('calls the operations in order when the source invokes the passed dispatch function', () => {
        const test = stream(source, operationA, operationB);
        test();
        expect(operationA.calls.allArgs()).toEqual([[1, jasmine.any(Function)], [2, jasmine.any(Function)]]);
        expect(operationB.calls.allArgs()).toEqual([[2, jasmine.any(Function)], [4, jasmine.any(Function)]]);
    });

    it('calls the sink with the result of the chain of operations', () => {
        const test = stream(source, operationA, operationB);
        test(sink);
        expect(sink.calls.allArgs()).toEqual([[6], [12]]);
    });

    it('call the sink with the values from the source if no operations are specified', () => {
        const test = stream(source);
        test(sink);
        expect(sink.calls.allArgs()).toEqual([[1], [2]]);
    });
});
