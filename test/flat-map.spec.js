import { flatMap } from '../src/flat-map';

describe('map', () => {
    let rule, operation, next;

    beforeEach(() => {
        rule = jasmine.createSpy('rule').and.returnValue(['response A', 'response B']);
        operation = flatMap(rule);
        next = jasmine.createSpy('next');
    });

    it('will call the provided rule function with the incoming message', () => {
        operation('message', next);
        expect(rule).toHaveBeenCalledWith('message');
    });

    it('will inject in the stream the elements of the result array of the rule function', () => {
        operation('message', next);
        expect(next.calls.allArgs()).toEqual([
            ['response A'],
            ['response B']
        ]);
    });
});
