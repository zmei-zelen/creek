import { filter } from '../src/filter';

describe('filter', () => {
    let condition, operation, next;

    beforeEach(() => {
        condition = jasmine.createSpy('condition');
        operation = filter(condition);
        next = jasmine.createSpy('next');
    });

    it('will call the provided condition function with the incoming message', () => {
        operation('message', next);
        expect(condition).toHaveBeenCalledWith('message');
    });

    it('will pass the input message back in the stream if the provided function returns true', () => {
        condition.and.returnValue(true);
        operation('message', next);
        expect(next).toHaveBeenCalledWith('message');
    });

    it('returns a stream operation that will not pass the input message if the provided function returns false', () => {
        condition.and.returnValue(false);
        operation('message', next);
        expect(next).not.toHaveBeenCalled();
    });
});
