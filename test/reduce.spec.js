import { reduce } from '../src/reduce';

describe('map', () => {
    let rule, operation, next;

    beforeEach(() => {
        rule = jasmine.createSpy('rule').and.returnValue('first result');
        operation = reduce(rule, 'default');
        next = jasmine.createSpy('next');
    });

    it('will call the rule function with the result of the previous call or a default value', () => {
        operation('message A', next);
        rule.and.returnValue('second result');
        operation('message B', next);
        rule.and.returnValue('third result');
        operation('message C', next);
        expect(rule.calls.allArgs()).toEqual([
            ['default', 'message A'],
            ['first result', 'message B'],
            ['second result', 'message C']
        ]);
    });

    it('will inject in the stream the result of the rule function', () => {
        operation('message A', next);
        rule.and.returnValue('second result');
        operation('message B', next);
        expect(next.calls.allArgs()).toEqual([
            ['first result'],
            ['second result']
        ]);
    });
});
