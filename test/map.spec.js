import { map } from '../src/map';

describe('map', () => {
    let rule, operation, next;

    beforeEach(() => {
        rule = jasmine.createSpy('rule').and.returnValue('transformed message');
        operation = map(rule);
        next = jasmine.createSpy('next');
    });

    it('will call the provided rule function with the incoming message', () => {
        operation('message', next);
        expect(rule).toHaveBeenCalledWith('message');
    });

    it('will inject the result of the rule function in the stream', () => {
        operation('message', next);
        expect(next).toHaveBeenCalledWith('transformed message');
    });
});
