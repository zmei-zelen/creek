import babel from 'rollup-plugin-babel';

export default {
    input: 'src/index.js',
    output: {
        file: 'lib/creek.umd.js',
        format: 'umd',
        name: 'creek'
    },
    plugins: [
        babel({
            presets: ['es2015-rollup'],
            plugins: ['external-helpers'],
            babelrc: false
        })
    ]
};
