function compose(composition, current) {
    return (message, next) => composition(message, result => current(result, next));
}

export function stream(source, ...operations) {
    const [first, ...rest] = operations;
    const composition = rest.reduce(compose, first || ((message, next) => next(message)));
    return handler => source(message => composition(message, handler || (() => {})));
}
