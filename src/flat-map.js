export function flatMap(rule) {
    return (message, next) => rule(message).forEach(result => next(result));
}
