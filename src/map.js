export function map(rule) {
    return (message, next) => next(rule(message));
}
