export function reduce(rule, initial) {
    let accumulator = initial;
    return (message, next) => next(accumulator = rule(accumulator, message));
}
