export function filter(condition) {
    return (message, next) => condition(message) && next(message);
}
